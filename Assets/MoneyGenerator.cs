using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyGenerator : MonoBehaviour
{
    [Tooltip("Time between money generation in seconds.")]
    public float generationInterval = 50f;
    [Tooltip("Amount of money to add to player each interval.")]
    public int generationAmount = 10;

    [HideInInspector]
    public float timer = 0f;

    private TreeGrowth treeGrowth;

    private void Awake() => treeGrowth = GetComponent<TreeGrowth>();

    private void Update() => Generation();

    private void Generation()
    {
        timer += Time.deltaTime;

        if (timer >= generationInterval)
        {
            timer = 0f;

            int amountToAdd = Mathf.FloorToInt(generationAmount * treeGrowth.GetPercentage());
            Inventory.instance.AddCurrency(amountToAdd);
        }
    }

}
