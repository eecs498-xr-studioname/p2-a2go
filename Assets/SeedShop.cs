using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeedShop : MonoBehaviour
{
    const string welcome = "<b>Welcome to the Seed Shop!</b>\n\n";

    [TextArea(15, 20)]
    public string text;

    [Header("Setup")]
    public Text textObject;
    public Text currencyObject;

    private void Start()
    {
        UpdateText();
    }

    private void Update()
    {
        UpdateCurrency();
    }

    private void UpdateText() => textObject.text = welcome + text;
    private void UpdateCurrency() => currencyObject.text = $"{Inventory.instance.currency} coins";

    public void BuySeed(Inventory.Seed seed, int cost)
    {
        if (Inventory.instance.currency < cost)
        {
            Debug.Log($"Not enough coins to buy {Inventory.SeedName(seed)}, have {Inventory.instance.currency} but need {cost} coins.");
            return;
        }

        Debug.Log($"Buying {Inventory.SeedName(seed)} for {cost} coins.");
        Inventory.instance.AddSeed(seed);
        Inventory.instance.SpendCurrency(cost);
        Debug.Log($"Have {Inventory.instance.currency} coins remaining.");
    }
}
