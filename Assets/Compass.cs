using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Compass : MonoBehaviour
{
    [Header("Setup")]
    public OrbitCamera orbitCamera;
    private void FixedUpdate()
    {
        transform.eulerAngles = new Vector3(0, 0, orbitCamera.direction_looking);
    }
}
