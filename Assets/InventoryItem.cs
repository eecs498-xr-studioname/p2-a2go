using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour
{
    public Inventory.Seed seed;

    [Header("Setup")]
    public Text number_text;

    public void UpdateInventoryItem()
    {
        number_text.text = Inventory.instance.seed_inventory[seed].ToString();
    }

    private void Start()
    {
        UpdateInventoryItem();
        Inventory.instance.updateInventory.AddListener(UpdateInventoryItem);
    }

    public void SpawnTree() => InventoryCanvas.instance.SpawnTree(seed);
    public void SpawnAcorn() => InventoryCanvas.instance.SpawnAcorn();
}
