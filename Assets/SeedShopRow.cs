using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeedShopRow : MonoBehaviour
{
    [Header("Seed Details")]
    public Inventory.Seed seed;
    public int seed_cost = 10; // default cost: 10 coins
    public Sprite seed_sprite;

    [Header("REQUIRED: Assign Seed Shop")]
    public SeedShop seedShop;

    [Header("Setup")]
    public Text nameText;
    public Text buyButtonText;
    public Button buyButton;
    public Image image;

    private Image buyButtonImage;

    private void Start()
    {
        buyButtonText.text = $"{seed_cost} coins";
        nameText.text = Inventory.SeedName(seed);
        image.sprite = seed_sprite;
        buyButtonImage = buyButton.gameObject.GetComponent<Image>();
    }

    private void Update()
    {
        buyButton.interactable = CanBuy();
        buyButtonImage.color = !CanBuy() ? Color.red : Color.white;
    }

    public bool CanBuy()
    {
        return Inventory.instance.unlocked_seed_types[seed] &&
            Inventory.instance.currency >= seed_cost;
    }

    public void Buy() => seedShop.BuySeed(seed, seed_cost);
}
