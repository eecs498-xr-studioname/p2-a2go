using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScene : MonoBehaviour
{
    public Scene sceneToSwitchTo;
    public Transform player;

    public enum Scene
    {
        exploration,
        interation,
    };

    public string GetSceneName(Scene scene)
    {
        switch (scene)
        {
            case Scene.exploration:
                return "exploration_scene";
            case Scene.interation:
                return "interaction_scene";
            default:
                return "";
        }
    }

    public void Switch()
    {
        var sceneName = GetSceneName(sceneToSwitchTo);
        if (sceneName == "interaction_scene" && TreeList.instance.list.Count != 0)
        {
            IsTree nearestTree = null;
            var nearestTreeDist = 999f;

            var trees = FindObjectsOfType<IsTree>();
            foreach (var tree in trees)
            {
                var dist = Vector3.Distance(tree.transform.position, player.position);
                if (dist < nearestTreeDist)
                {
                    nearestTree = tree;
                    nearestTreeDist = dist;
                }
            }

            if (nearestTree != null)
            {
                TreeList.instance.nearestTreeID = nearestTree.ID;
            }
        }
        else
        {
            TreeList.instance.nearestTreeID = "";
        }
        SceneManager.LoadScene(sceneName);
    }
}
