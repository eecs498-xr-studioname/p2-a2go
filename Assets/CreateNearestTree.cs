using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateNearestTree : MonoBehaviour
{
    private void Start()
    {
        if (TreeList.instance.nearestTreeID != "")
        {
            var treeID = TreeList.instance.nearestTreeID;

            foreach (var tree in TreeList.instance.list)
            {
                if (tree.ID == treeID)
                {
                    InstantiateTree(tree);
                    break;
                }
            }
        }
    }

    private void InstantiateTree(TreeList.TreeObject tree)
    {
        var ar_camera = GameObject.Find("/AR Session Origin/AR Camera");
        Transform camera_t = ar_camera.transform;
        Vector3 pos = camera_t.position;
        pos += camera_t.forward * 15f;
        pos += camera_t.right * 4f;
        var treeGo = tree.Create(pos, Quaternion.identity);
    }
}
