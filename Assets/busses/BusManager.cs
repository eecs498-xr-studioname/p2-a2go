using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BusManager : MonoBehaviour
{
    [Tooltip("Counted in real life seconds.")]
    public float timeBetweenGetCalls = 300; // in seconds (5 minutes)

    [Header("Setup")]
    public GameObject uiCanvas;
    public Text uiText;
    public FetchBusLocation fetchBusLocation;

    private bool runBusSystem = true;

    private void Start()
    {
        if (Application.isEditor)
        {
            runBusSystem = false;
            uiCanvas.SetActive(true);
        }
        RunProgram();
    }

    private void Update() => ShowEditorText();

    private void ShowEditorText()
    {
        if (Application.isEditor)
        {
            if (Input.GetKey(KeyCode.Alpha1))
            {
                uiText.text = "Press '2' to Disable Blue Busses";
                runBusSystem = true;
            }
            else if (Input.GetKey(KeyCode.Alpha2))
            {
                uiText.text = "Press '1' to Enable Blue Busses";
                runBusSystem = false;
            }
        }
    }

    private void RunProgram() => StartCoroutine(Driver());

    private IEnumerator Driver()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(timeBetweenGetCalls);

            if (runBusSystem)
            {
                StartCoroutine(fetchBusLocation.GetVehiclesIEnum());
            }
        }
    }
}
