[System.Serializable]
public class ErrorType
{
    public string rt;
    public string msg;

    public ErrorType(string rt, string msg)
    {
        this.rt = rt;
        this.msg = msg;
    }

    public override string ToString()
    {
        return $"Error: route {rt}\n{msg}";
    }
}
