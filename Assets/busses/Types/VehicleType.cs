using System.Collections.Generic;

[System.Serializable]
public class VehicleType
{
    public List<BusType> vehicle;
    public List<ErrorType> error;

    public VehicleType(List<BusType> vehicle, List<ErrorType> error)
    {
        this.vehicle = vehicle;
        this.error = error;
    }

    public override string ToString()
    {
        return $"List<Bus> size: {vehicle.Count}, List<Err> size: {error.Count}";
    }
}