[System.Serializable]
public class BustimeResponseType
{
    public VehicleType bustimeResponse;

    public BustimeResponseType(VehicleType bustimeResponse)
    {
        this.bustimeResponse = bustimeResponse;
    }

    public override string ToString()
    {
        return bustimeResponse.ToString();
    }
}
