[System.Serializable]
public class BusType
{
    public string vid;
    public string tmstmp;
    public string lat;
    public string lon;
    public string hdg;
    public int pid;
    public string rt;
    public string des;
    public int pdist;
    public bool dly;
    public int spd;
    public string tatripid;
    public string origtatripno;
    public string tablockid;
    public string zone;
    public int mode;
    public string psgld;
    public int stst;
    public string stsd;

    public BusType(string vid, string tmstmp, string lat, string lon, string hdg, int pid, string rt, string des,
        int pdist, bool dly, int spd, string tatripid, string origtatripno, string tablockid, string zone,
        int mode, string psgld, int stst, string stsd)
    {
        this.vid = vid;
        this.tmstmp = tmstmp;
        this.lat = lat;
        this.lon = lon;
        this.hdg = hdg;
        this.pid = pid;
        this.rt = rt;
        this.des = des;
        this.pdist = pdist;
        this.dly = dly;
        this.spd = spd;
        this.tatripid = tatripid;
        this.origtatripno = origtatripno;
        this.tablockid = tablockid;
        this.zone = zone;
        this.mode = mode;
        this.psgld = psgld;
        this.stst = stst;
        this.stsd = stsd;
    }

    public override string ToString()
    {
        return $"Vehicle {rt} #{vid} at (lat: {lat}, lon: {lon}).";
    }
}
