using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsBus : FollowTarget
{
    public Transform view;

    public void UpdateLocalPos(Vector3 newLocalPos, float timeBeforeUpdate)
    {
        ease = 1 / timeBeforeUpdate;
        target.localPosition = newLocalPos;
    }

    public override void MoveTowardsDesiredPosition()
    {
        // Calculate desired_position.
        Vector3 desired_position = target.position + offset;

        // Lerp towards desired_postion.
        view.transform.position = Vector3.Lerp(view.transform.position, desired_position, ease * Time.deltaTime);

        // look towards target
        view.transform.LookAt(target);
    }

    public void MoveToDesiredPosition()
    {
        // Calculate desired_position.
        Vector3 desired_position = target.position + offset;

        // Move to desired_postion.
        view.transform.position = desired_position;
    }
}
