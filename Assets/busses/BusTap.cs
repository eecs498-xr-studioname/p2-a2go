using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BusTap : MonoBehaviour
{
    [HideInInspector] public string rt;
    [HideInInspector] public string vid;

    [Header("Setup")]
    public Canvas infoUI;
    public Text infoUIText;

    private Inventory.Seed unlocked_seed = Inventory.Seed.none;

    public void Tapped()
    {
        unlocked_seed = Inventory.instance.UnlockRandomSeed();
        Inventory.instance.AddSeed(unlocked_seed);

        UpdateUI();
        infoUI.gameObject.SetActive(true);
    }

    private void UpdateUI()
    {
        infoUIText.text = $"<b>{GetBusRT(rt)} Bus #{vid}</b>\n\n";
        infoUIText.text += $"You've obtained a <b>{Inventory.SeedName(unlocked_seed)}!</b>\n\n";
    }

    private string GetBusRT(string rt)
    {
        return rt switch
        {
            "BB" => "Bursley-Baits",
            "CN" => "Commuter North",
            "CS" => "Commuter South",
            "NW" => "Northwood",
            "NX" => "Northwood Express",
            _ => "",
        };
    }
}
