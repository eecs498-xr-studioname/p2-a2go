using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class FetchBusLocation : MonoBehaviour
{
    /*
     * UMich BusTime API Link
     * https://mbus.ltp.umich.edu/bustime/api/v3/<method>?key=Hi2BXN3C36DGbvTAFur4FkXyv&format=json<other-queries>
     */

    /*
     * GetRoutes
     * https://mbus.ltp.umich.edu/bustime/api/v3/getroutes?key=Hi2BXN3C36DGbvTAFur4FkXyv&format=json
     * 
     * Use the getroutes request to retrieve the set of routes serviced by the system.
     */

    /*
     * GetVehicles
     * https://mbus.ltp.umich.edu/bustime/api/v3/getvehicles?key=Hi2BXN3C36DGbvTAFur4FkXyv&format=json&rt=BB,CN,CS,NW
     * 
     * Use the getvehicles request to retrieve vehicle information (i.e., locations) of all or a subset of
     * vehicles currently being tracked by BusTime.
     * - Use the rt parameter to retrieve information for vehicles currently running one or more of the specified routes.
     * 
     * Northwood route "NW":            &rt=NW
     * Northwood Express route "NX":    &rt=NX
     * Bursley-Baits route "BB":        &rt=BB
     * Commuter South route "CS":       &rt=CS
     * Commuter North route "CN":       &rt=CN
     */

    [Header("Setup")]
    public RunningBusses runningVehicles;

    private bool requestInProgress = false;

    private readonly string getVehiclesPath = $"https://mbus.ltp.umich.edu/bustime/api/v3/getvehicles?key=Hi2BXN3C36DGbvTAFur4FkXyv&format=json&rt=BB,CN,CS,NW,NX";

    /* Sending Web Requests in Unity (https://www.youtube.com/watch?v=K9uVHI645Pk) */
    public IEnumerator GetVehiclesIEnum()
    {
        /* Handle Web Request and JSON data and populate the runningVehicles list. */
        if (!requestInProgress)
        {
            requestInProgress = true; // stop a subsequent request

            // GET Request
            var getRequest = GETRequest(getVehiclesPath);

            // wait to recieve response
            yield return getRequest.SendWebRequest();

            // process JSON data
            var jsonText = Regex.Replace(getRequest.downloadHandler.text, "bustime-response", "bustimeResponse");
            var data = JsonConvert.DeserializeObject<BustimeResponseType>(jsonText);

            runningVehicles.Replace(data.bustimeResponse.vehicle);

            requestInProgress = false; // allow another request
            Debug.Log("Finished API Call");
        }
    }

    private UnityWebRequest GETRequest(string path)
    {
        UnityWebRequest request = new(path);

        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        return request;
    }

    private enum RequestType
    {
        GET = 0,
    }
}
