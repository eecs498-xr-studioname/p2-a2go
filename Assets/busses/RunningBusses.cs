using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.Location;
using Mapbox.Utils;

public class RunningBusses : MonoBehaviour
{
    public GameObject busPrefab;

    [Header("Setup")]
    public BusManager busManager;

    private Dictionary<string, (string, string)> busses;
    private Dictionary<string, GameObject> busGameObjects;

    public RunningBusses()
    {
        busses = new();
        busGameObjects = new();
    }

    public void Replace(List<BusType> newList)
    {
        List<BusType> bussesToAdd = new(newList);
        List<string> bussesToRemove = busses.Keys.ToList();
        List<BusType> bussesToUpdate = new();

        foreach (var bus in newList)
        {
            if (busses.ContainsKey(bus.vid))
            {
                bussesToAdd.Remove(bus);
                bussesToRemove.Remove(bus.vid);
                bussesToUpdate.Add(bus);
            }
        }

        /* Add New Busses */
        foreach (var bus in bussesToAdd)
        {
            // Add Bus data to busses dict
            busses[bus.vid] = (bus.lat, bus.lon);

            // Create Bus GameObject
            var busGO = GameObject.Instantiate(busPrefab);
            busGO.transform.parent = this.transform;

            // BusTap fields
            var busTap = busGO.GetComponent<BusTap>();
            busTap.vid = bus.vid;
            busTap.rt = bus.rt;

            // add busGO to busGameObject dict
            busGameObjects.Add(bus.vid, busGO);

            // Update Bus Position
            UpdateBusPosition(bus.vid, true);
        }

        /* Remove Busses that are no longer active */
        foreach (var key in bussesToRemove)
        {
            Destroy(busGameObjects[key]);
            busses.Remove(key);
        }

        foreach(var bus in bussesToUpdate)
        {
            busses[bus.vid] = (bus.lat, bus.lon);
            UpdateBusPosition(bus.vid);
        }
    }

    public void UpdateBusPosition(string key, bool instant = false)
    {
        var map = LocationProviderFactory.Instance.mapManager;
        var (lat, lon) = busses[key];
        var latLonCoord = new Vector2d(float.Parse(lat), float.Parse(lon));
        //Debug.Log(map.GeoToWorldPosition(latLonCoord));
        var isBus = busGameObjects[key].GetComponent<IsBus>();
        var worldPos = map.GeoToWorldPosition(latLonCoord);
        isBus.UpdateLocalPos(worldPos, busManager.timeBetweenGetCalls);
        if (instant)
            isBus.MoveToDesiredPosition();
    }
}
