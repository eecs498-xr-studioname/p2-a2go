using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Create_squ : MonoBehaviour
{
    //public InventoryCanvas inventory;
    // Start is called before the first frame update
    public GameObject squ_int;
    public AudioSource audioSource;
    public AudioClip battle;
    void Start()
    {
        if (InventoryCanvas.instance.has_squ)
        {
            GameObject ar_camera = GameObject.Find("/AR Session Origin/AR Camera");
            Transform camera_t = ar_camera.transform;
            Vector3 pos = camera_t.position;
            pos += camera_t.forward * 20;
            GameObject new_squ = Instantiate(squ_int, pos, Quaternion.identity);
            audioSource.clip = battle;
            audioSource.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
