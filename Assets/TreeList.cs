using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeList : MonoBehaviour
{
    /* Create Singleton instance and DontDestroyOnLoad */
    public static TreeList instance;
    void Start()
    {
        // If there is an instance, and it's not me, delete myself.
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }
    }

    public List<TreeObject> list = new();
    [HideInInspector] public string nearestTreeID = "";

    public struct TreeObject
    {
        public string ID;
        public Inventory.Seed treeSeed;
        public GameObject treeGameObject;
        public float moneyGeneratorTimer;
        public float treeGrowthTimer;
        public int treeGrowthStage;

        public TreeObject(Inventory.Seed seed, GameObject treeObject)
        {
            ID = System.Guid.NewGuid().ToString();
            treeSeed = seed;
            treeGameObject = treeObject;
            moneyGeneratorTimer = 0f;
            treeGrowthTimer = 0f;
            treeGrowthStage = 1;
        }

        public void UpdateValues(float genTimer, float growthTimer, int growthStage)
        {
            moneyGeneratorTimer = genTimer;
            treeGrowthTimer = growthTimer;
            treeGrowthStage = growthStage;
        }

        public void SetValues(GameObject tree)
        {
            // set IsTree value(s)
            IsTree isTree = tree.GetComponent<IsTree>();
            isTree.ID = ID;

            // set MoneyGenerator value(s)
            MoneyGenerator moneyGen = tree.GetComponent<MoneyGenerator>();
            moneyGen.timer = moneyGeneratorTimer;

            // set TreeGrowth value(s)
            TreeGrowth treeGrowth = tree.GetComponent<TreeGrowth>();
            treeGrowth.timer = treeGrowthTimer;
            treeGrowth.current_step = treeGrowthStage;
        }

        public GameObject Create(Vector3 pos, Quaternion rot)
        {
            GameObject tree = Instantiate(treeGameObject);
            tree.transform.SetLocalPositionAndRotation(pos, rot);

            SetValues(tree);

            return tree;
        }

        public GameObject Create(Vector3 pos, Quaternion rot, Vector3 scale)
        {
            GameObject tree = Instantiate(treeGameObject);
            tree.transform.SetLocalPositionAndRotation(pos, rot);
            tree.transform.localScale = scale;

            SetValues(tree);

            return tree;
        }
    }
}
