using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using TMPro;

public class Inventory : MonoBehaviour
{
    /* Create Singleton instance and DontDestroyOnLoad */
    public static Inventory instance;
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }
    }

    public bool imageadd = false;

    /* Money */
    public int currency { get; private set; } = 100;
    public void SpendCurrency(int cost) => currency -= cost;
    public void AddCurrency(int pay) => currency += pay;

    public TextMeshProUGUI text;
    private void Update()
    {
        text.text = currency.ToString();
    }

    /* Seeds */
    public Dictionary<Seed, int> seed_inventory = new()
    {
        {Seed.acorn, 5},
        {Seed.alder, 0 },
        {Seed.maple, 0 },
        {Seed.oak, 5 },
        {Seed.palm, 0 },
        {Seed.pine, 0 },
        {Seed.spooky, 2 },
    };

    /* unlocked seed types */
    public Dictionary<Seed, bool> unlocked_seed_types = new()
    {
        {Seed.acorn,true },
        {Seed.alder, false },
        {Seed.maple, false },
        {Seed.oak, true },
        {Seed.palm, false },
        {Seed.pine, false },
        {Seed.spooky, true },
    };

    public Seed UnlockRandomSeed()
    {
        var keys = unlocked_seed_types.Keys.ToArray();
        int rng = Random.Range(0, keys.Length);
        unlocked_seed_types[keys[rng]] = true;
        //Debug.Log($"unlocked {keys[rng]}");
        return keys[rng];
    }

    [HideInInspector] public UnityEvent updateInventory = new();

    public void AddSeed(Seed seed)
    {
        seed_inventory[seed] += 1;
        updateInventory.Invoke();
    }
    public void UseSeed(Seed seed)
    {
        seed_inventory[seed] -= 1;
        updateInventory.Invoke();
    }

    public enum Seed
    {
        alder,
        maple,
        oak,
        palm,
        pine,
        spooky,
        none,
        acorn,
    }

    public static string SeedName(Seed seed)
    {
        return seed switch
        {
            Seed.acorn => "Acorn",
            Seed.alder => "Alder Seed",
            Seed.maple => "Maple Seed",
            Seed.oak => "Oak Seed",
            Seed.palm => "Palm Seed",
            Seed.pine => "Pine Seed",
            Seed.spooky => "Spooky Seed",
            Seed.none => "",
            _ => "",
        };
    }
}
