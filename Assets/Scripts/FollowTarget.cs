using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    [Header("Follow Target")]
    public Transform target;
    public Vector3 offset;
    [Tooltip("Ease the jerkiness of moving the object using lerp.")]
    public float ease = 0.1f;
    public bool angel_in_direction = true;
    private Vector3 previous_position = Vector3.zero;

    public virtual void Update() => MoveTowardsDesiredPosition();

    public virtual void MoveTowardsDesiredPosition()
    {
        // Calculate desired_position.
        Vector3 desired_position = target.position + offset;

        // Lerp towards desired_postion.
        transform.position = Vector3.Lerp(transform.position, desired_position, ease);
        if (angel_in_direction)
        {
            Vector3 position_delta = transform.position - previous_position;
            if(position_delta.magnitude > 0.01f)
            {
                transform.forward = position_delta;
            }
        }
        previous_position = transform.position;
    }
}
