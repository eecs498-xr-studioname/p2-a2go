using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mapbox.Utils;
using Mapbox.Unity.Map;
public class InventoryCanvas : MonoBehaviour
{
    
    [Header("Tree GameObjects to Spawn")]
    public GameObject oakTree;
    public GameObject pineTree;
    public GameObject alderTree;
    public GameObject mapleTree;
    public GameObject palmTree;
    public GameObject spookyTree;
    public GameObject acorn;
    public GameObject squ_ex;
    public GameObject suq_int;

    [Header("Setup")]
    public AudioClip spawn_sound_effect;
    public AudioClip throw_sound;
    public bool has_squ;

    /* Create Singleton instance and DontDestroyOnLoad */
    public static InventoryCanvas instance;
    void Start()
    {
        // If there is an instance, and it's not me, delete myself.
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
            has_squ = false;
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }
    }

    public void SpawnAcorn()
    {
        Debug.Log("Spawn Acorn");
        if(SceneManager.GetActiveScene().name == "interaction_scene" && Inventory.instance.seed_inventory[Inventory.Seed.acorn] > 0)
        {
            Debug.Log("Spawn Acorn");
            GameObject ar_camera = GameObject.Find("/AR Session Origin");
            Transform camera_t = ar_camera.transform;
            //GameObject inter = GameObject.Find("/ar_cursor");
            //Transform cursor = inter.transform;
            GameObject new_acorn = Instantiate(acorn, camera_t.position, Quaternion.identity);
            //Vector3 diff = camera_t.position -  cursor.position;
            new_acorn.GetComponent<Rigidbody>().velocity = Camera.main.transform.forward * 20;
            Debug.Log("The camera pos is :" + camera_t.position);
            Debug.Log("The camera forward is: " + camera_t.forward);
            Inventory.instance.UseSeed(Inventory.Seed.acorn);
            AudioSource.PlayClipAtPoint(throw_sound,Camera.main.transform.position);
            Debug.Log("Create");

        }
        else
        {
            Debug.Log("Here is exploration for acorn");
        }
    }

    public void SpawnTree(Inventory.Seed seed)
    {
        if (SceneManager.GetActiveScene().name == "interaction_scene"
            && Inventory.instance.seed_inventory[seed] > 0
            && Inventory.instance.unlocked_seed_types[seed])
        {
            
            TreeList.TreeObject tree = new(seed, GetTreeGameObject(seed));

            GameObject inter = GameObject.Find("/ar_cursor");
            Transform cursor = inter.transform;
            Debug.Log("The pos of cursor " + cursor.position);

            // Spawn tree GameObject
            tree.Create(cursor.position, cursor.rotation);

            AudioSource.PlayClipAtPoint(spawn_sound_effect, Camera.main.transform.position);

            // remove seed from inventory
            Inventory.instance.UseSeed(seed);

            // update trees log
            TreeList.instance.list.Add(tree);
            Debug.Log(TreeList.instance.list[TreeList.instance.list.Count-1].treeGameObject);
            has_squ = true;
        }
        else
        {
            Debug.Log("Can't spawn trees in the Exploration Scene.");
        }
    }

    private GameObject GetTreeGameObject(Inventory.Seed seed)
    {
        switch (seed)
        {
            case Inventory.Seed.alder:
                return alderTree;
            case Inventory.Seed.maple:
                return mapleTree;
            case Inventory.Seed.oak:
                return oakTree;
            case Inventory.Seed.palm:
                return palmTree;
            case Inventory.Seed.pine:
                return pineTree;
            case Inventory.Seed.spooky:
                return spookyTree;
            default:
                return new();
        }
    }

    public void Create()
    {
        /* Instantiate tree GameObjects in Exploration View. */
        for (int i = 0; i < TreeList.instance.list.Count; i++)
        {
            // generate and apply offset so that trees are not on top of eachother
            Vector3 rnd = new(Random.Range(-20.0f, 20.0f), 0f, Random.Range(-20.0f, 20.0f));

            // create tree
            TreeList.instance.list[i].Create(rnd, Quaternion.identity, new Vector3(2f, 2f, 2f));
        }
        if (has_squ)
        {
            Vector3 rnd_1 = new(Random.Range(-20.0f, 20.0f), 0f, Random.Range(-20.0f, 20.0f));
            GameObject new_squ = Instantiate(squ_ex, rnd_1, Quaternion.identity);
        }
    }
}
