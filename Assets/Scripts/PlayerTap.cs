using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTap : MonoBehaviour
{
    public float max_landmark_access_dist = 10f;
    public float max_bus_access_dist = 30f;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            //Debug.Log(ray.origin);
            Debug.DrawRay(ray.origin, ray.direction * 1000f, Color.green);
            
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000f))
            {
                var dist = Vector3.Distance(transform.position, hit.transform.position);
                if (hit.transform.gameObject.CompareTag("Landmark") && (dist < max_landmark_access_dist))
                {
                    hit.transform.GetComponent<IsLandmark>().EnableLandmarkUI();
                }
                else if (hit.transform.gameObject.CompareTag("Bus") && (dist < max_bus_access_dist))
                {
                    hit.transform.parent.GetComponent<BusTap>().Tapped();
                }
            }
        }
    }
}
