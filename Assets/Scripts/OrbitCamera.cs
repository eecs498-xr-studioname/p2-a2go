using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OrbitCamera : FollowTarget
{
    [Header("Horizontal Camera Movement (Rotate around Player)")]
    [Tooltip("Amplitude of the circle path that the camera follows in rotation.")]
    public float amplitude = 20f;
    [Tooltip("How fast the camera rotates around the player.")]
    public float horizontalSensitivity = 5f;

    [Header("Vertical Camera Movment")]
    [Tooltip("How fast the camera zooms in and out.")]
    public float verticalSensitivity = 2f;
    [Tooltip("Closest zoom of the camera.")]
    public float minAngle = 60f;
    [Tooltip("Farthest Zoom for the camera.")]
    public float maxAngle = 120f;

    [Header("Setup")]
    public GameObject direction_tracker;

    [HideInInspector]
    public float direction_looking;

    private float h_value = 0;
    private float y_value = 100;

    public override void Update()
    {
        RotateCamera();

        // Call FollowTarget's Update, which handles following the player movement.
        base.Update();
    }

    public void RotateCamera()
    {
        if (Input.GetMouseButton(0))
        {
            /*
             * Get horizontal and vertical mouse inputs.
             * 
             * Horizontal movement rotates the camera around the central player object.
             * Vertical movement zooms the camera in and out.
             * 
             * Mouse inputs also work for touch screen.
             */
            h_value += Time.deltaTime * horizontalSensitivity * Input.GetAxis("Mouse X");
            y_value += Time.deltaTime * verticalSensitivity * 100 * -1 * Input.GetAxis("Mouse Y");
            y_value = Mathf.Clamp(y_value, minAngle, maxAngle);

            // Calculate new Vector3 values.
            float x = Mathf.Cos(h_value) * amplitude;
            float y = y_value;
            float z = Mathf.Sin(h_value) * amplitude;

            // Update transform.position and offset values.
            transform.position = new Vector3(x, y, z) + target.position;
            offset = transform.position - target.position;

            // Rotate the camera to look at the central player object.
            transform.LookAt(target);

            // Update Direction Tracker
            direction_tracker.transform.position = new Vector3(x + target.position.x, 0, z + target.position.z);
            direction_tracker.transform.LookAt(target);
            direction_looking = Vector3.SignedAngle(Vector3.forward, direction_tracker.transform.forward, Vector3.up);
        }
    }
}
