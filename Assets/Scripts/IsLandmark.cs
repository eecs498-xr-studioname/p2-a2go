using UnityEngine;
using Mapbox.Utils;
using Mapbox.Unity.Map;
using UnityEngine.UI;

public class IsLandmark : MonoBehaviour
{
    [Header("Location")]
    public Vector2 lat_lon_coordinate;
    public GameObject map;

    [Header("UI")]
    public string landmark_name;
    public Sprite landmark_img;
    [TextArea(15, 20)]
    public string landmark_description;

    [Header("Prefab Setup")]
    public GameObject landmark_object;
    public GameObject landmark_ui;
    public Text landmark_ui_text;
    public Image landmark_ui_img;

    private AbstractMap _abstractMap;

    private Inventory.Seed unlocked_seed = Inventory.Seed.none;

    private void Start()
    {
        _abstractMap = map.GetComponent<AbstractMap>();
        _abstractMap.OnInitialized += SetLandmarkLocation;
    }

    public void SetLandmarkLocation()
    {
        // Set Location from Lattitude and Longitude
        Vector3 world_position = _abstractMap.GeoToWorldPosition(new Vector2d(lat_lon_coordinate.x, lat_lon_coordinate.y));
        transform.position = world_position;

        // Set the GameObject Active
        landmark_object.SetActive(true);
    }

    public void EnableLandmarkUI()
    {
        unlocked_seed = Inventory.instance.UnlockRandomSeed();
        Inventory.instance.AddSeed(unlocked_seed);
        //Debug.Log(unlocked_seed);

        // Update the UI
        UpdateLandmarkUIText();
        UpdateLandmarkImage();

        landmark_ui.SetActive(true);
    }

    private void UpdateLandmarkUIText()
    {
        landmark_ui_text.text = $"<b>{landmark_name}</b>\n\n";
        landmark_ui_text.text += landmark_description + "\n\n";
        landmark_ui_text.text += $"You've obtained a <b>{Inventory.SeedName(unlocked_seed)}!</b>\n\n";
    }

    private void UpdateLandmarkImage()
    {
        landmark_ui_img.sprite = landmark_img;
    }
}
