﻿namespace Mapbox.Unity.Location
{
	using System;
	using System.Collections.Generic;
	using System.Globalization;
	using System.IO;
	using Mapbox.Unity.Utilities;
	using Mapbox.Utils;
	using UnityEngine;
	
	/// <summary>
	/// <para>
	/// The EditorLocationProvider is responsible for providing mock location data via log file collected with the 'LocationProvider' scene
	/// </para>
	/// </summary>
	public class EditorLocationProviderLocationLog : AbstractEditorLocationProvider
	{


		/// <summary>
		/// The mock "latitude, longitude" location, respresented with a string.
		/// You can search for a place using the embedded "Search" button in the inspector.
		/// This value can be changed at runtime in the inspector.
		/// </summary>
		[SerializeField]
		private TextAsset _locationLogFile;


		private LocationLogReader _logReader;
		private IEnumerator<Location> _locationEnumerator;
		
		const float diag_lat = 42.276987173705365f;
		const float diag_lon = -83.73821031966807f;


#if UNITY_EDITOR
		protected override void Awake()
		{
			base.Awake();
			_logReader = new LocationLogReader(_locationLogFile.bytes);
			_locationEnumerator = _logReader.GetLocations();

			// Set inital location to the Diag
			Location new_location = new Location();
			new_location.LatitudeLongitude = new Vector2d(diag_lat, diag_lon);
			_currentLocation = new_location;
		}
#endif


		private void OnDestroy()
		{
			if (null != _locationEnumerator)
			{
				_locationEnumerator.Dispose();
				_locationEnumerator = null;
			}
			if (null != _logReader)
			{
				_logReader.Dispose();
				_logReader = null;
			}
		}

		public float movement_magnitude = 0.00001f;

        public void Update() => DebugMovement();

		private void DebugMovement()
        {
			Vector2d movement_delta = Vector2d.zero;

			// Handle Vertical (Latitude) Movement
			if (Input.GetKey(KeyCode.UpArrow))
				movement_delta += new Vector2d(1, 0);
			if (Input.GetKey(KeyCode.DownArrow))
				movement_delta += new Vector2d(-1, 0);

			// Handle Horizontal (Longitude) Movement
			if (Input.GetKey(KeyCode.RightArrow))
				movement_delta += new Vector2d(0, 1);
			if (Input.GetKey(KeyCode.LeftArrow))
				movement_delta += new Vector2d(0, -1);

			// Normalize movement for constant movement speed.
			movement_delta = movement_delta.normalized * movement_magnitude;

			// Update location to move player.
			_currentLocation.LatitudeLongitude += movement_delta;
		}


        protected override void SetLocation()
		{
			if (null == _locationEnumerator) { return; }

			// no need to check if 'MoveNext()' returns false as LocationLogReader loops through log file
			// _locationEnumerator.MoveNext();
			// _currentLocation = _locationEnumerator.Current;

			// Location new_location = new Location();
			// new_location.LatitudeLongitude = new Vector2d(42.276987173705365, -83.73821031966807); // the Diag location
			// _currentLocation = new_location;

			// Debug.Log("Set Location [" + _currentLocation.LatitudeLongitude + "]");
		}
	}
}
