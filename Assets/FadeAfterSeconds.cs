using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeAfterSeconds : MonoBehaviour
{
    public float timeToFade = 5f;

    private void Start()
    {
        StartCoroutine(FadeAlphaToZero(GetComponent<Text>(), timeToFade));
    }

    /* 
     * IEnumerator function that fades the transparency of a GameObject to black over time.
     * 
     * Code from:
     * https://forum.unity.com/threads/how-to-fade-out-object-after-some-time-on-collision.911225/ 
     */
    IEnumerator FadeAlphaToZero(Text textObject, float duration)
    {
        Color startColor = textObject.color;
        Color endColor = new(startColor.r, startColor.g, startColor.b, 0);

        float timer = 0;
        while (timer < duration)
        {
            timer += Time.deltaTime;
            textObject.color = Color.Lerp(startColor, endColor, timer / duration);
            yield return null;
        }
    }
}
