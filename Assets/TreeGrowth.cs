using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGrowth : MonoBehaviour
{
    [Tooltip("Time before growth in seconds.")]
    public float growthInterval = 50f;
    [Tooltip("Amount of steps in growth.")]
    public int growthSteps = 10;

    [HideInInspector]
    public float timer = 0f;
    [HideInInspector]
    public int current_step = 1;

    private Vector3 maxScale;

    public GameObject treeObject;

    private void Start()
    {
        maxScale = treeObject.transform.localScale;
        UpdateLocalScale();
    }

    private void Update() => Grow();

    private void Grow()

    {
        if (current_step <= growthSteps)
        {
            timer += Time.deltaTime;

            if (timer >= growthInterval)
            {
                timer = 0f;
                current_step += 1;
                UpdateLocalScale();
            }
        }
    }

    private void UpdateLocalScale()
    {
        float factor = GetPercentage();
        treeObject.transform.localScale = new Vector3(maxScale.x * factor, maxScale.y * factor, maxScale.z * factor);
    }

    public float GetPercentage() => (float)current_step / (float)growthSteps;
}
