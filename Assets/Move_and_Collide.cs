using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_and_Collide : MonoBehaviour
{
    public AudioSource audio;
    public AudioClip inter;
    public AudioClip destroy_sound;
    public Vector3 rot = new(0,0,0);
    public Vector3 move = new(0,0,0);
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(rot);
        this.transform.Translate(move);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collide Trigg happen!");
        InventoryCanvas.instance.has_squ = false;
        AudioSource.PlayClipAtPoint(destroy_sound, Camera.main.transform.position);
        //Destroy(other.gameObject);
        Destroy(this.gameObject);
        Destroy(GameObject.Find("/background_music"));
        AudioSource.PlayClipAtPoint(inter, Camera.main.transform.position);

    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collide happen!");
        InventoryCanvas.instance.has_squ = false;
        AudioSource.PlayClipAtPoint(destroy_sound, Camera.main.transform.position);
        //Destroy(collision.gameObject);
        Destroy(this.gameObject);
        Destroy(GameObject.Find("/background_music"));
        AudioSource.PlayClipAtPoint(inter, Camera.main.transform.position);
    }
}
