using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Play_Stop : MonoBehaviour
{
    Animator ami;
    Vector3 pre;
    // Start is called before the first frame update
    void Start()
    {
        ami = GetComponent<Animator>();
        pre = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 delta = transform.position - pre;
        float base_speed = 4.0f;
        float ami_speed = base_speed * delta.magnitude;
        ami.speed = ami_speed;
        pre = transform.position;
    }
}
